package main

import (
	"bytes"
	"encoding/binary"
	"errors"
	"flag"
	"fmt"
	"time"

	"github.com/goburrow/modbus"
)

// Function Cheat sheet :
//
// 1	func (mb *client) ReadCoils(address, quantity uint16) (results []byte, err error)
// 2	func (mb *client) ReadDiscreteInputs(address, quantity uint16) (results []byte, err error)
// 3	func (mb *client) ReadHoldingRegisters(address, quantity uint16) (results []byte, err error)
// 4	func (mb *client) ReadInputRegisters(address, quantity uint16) (results []byte, err error)
// 5	func (mb *client) WriteSingleCoil(address, value uint16) (results []byte, err error)
// 6	func (mb *client) WriteSingleRegister(address, value uint16) (results []byte, err error)
// 15	func (mb *client) WriteMultipleCoils(address, quantity uint16, value []byte) (results []byte, err error)
// 16	func (mb *client) WriteMultipleRegisters(address, quantity uint16, value []byte) (results []byte, err error)
// 22	(mb *client) MaskWriteRegister(address, andMask, orMask uint16) (results []byte, err error)
// 23	(mb *client) ReadWriteMultipleRegisters(readAddress, readQuantity, writeAddress, writeQuantity uint16,
//						value []byte) (results []byte, err error)
// 24	(mb *client) ReadFIFOQueue(address uint16) (results []byte, err error)

// BytesToUint16Array converts an array of bytes to an array of
// uint16 using the given byte order.
func BytesToUint16Array(bx []byte, order binary.ByteOrder) ([]uint16, error) {
	l := len(bx)
	if l < 2 || l&1 != 0 {
		return nil, errors.New("Wrong number of bytes for []uint16 conversion")
	}
	buf := bytes.NewReader(bx)
	l = l / 2
	var result []uint16
	result = make([]uint16, l, l)
	for x := 0; x < l; x += 1 {
		err := binary.Read(buf, order, &result[x])
		if err != nil {
			return nil, err
		}
	}
	return result, nil
}

func ModbusReadUint16registers(client modbus.Client, addr uint16, length uint16) (res []uint16, err error) {
	v, err := client.ReadHoldingRegisters(addr, length)
	if err != nil {
		return
	}
	res, err = BytesToUint16Array(v, binary.BigEndian)
	return
}

func main() {

	host := flag.String("host", "idec:502", "Hostname and port to connect to")
	addr := flag.Uint("addr", 0, "Address of first register to read from.")
	length := flag.Uint("length", 1, "Number of registers to read.")
	repeat := flag.Uint("repeat", 1, "Number of times to read")
	delay := flag.String("delay", "50ms", "Delay between reads")
	flag.Parse()

	handler := modbus.NewTCPClientHandler(*host)
	handler.Timeout = 10 * time.Second
	handler.SlaveId = 0xFF
	//handler.Logger = log.New(os.Stdout, "test: ", log.LstdFlags)

	err := handler.Connect()
	if err != nil {
		fmt.Printf("Connect failed: %v\n", err)
		return
	}
	defer handler.Close()
	client := modbus.NewClient(handler)

	d, err := time.ParseDuration(*delay)
	if err != nil {
		fmt.Printf("Invalid delay value: %s\n", *delay)
		return
	}
	for x := 0; x < int(*repeat); x += 1 {
		ts := time.Now()
		v, err := ModbusReadUint16registers(client, uint16(*addr), uint16(*length))
		if err != nil {
			fmt.Printf("ModbusRead failed: %v\n", err)
			return
		}
		fmt.Printf("%s %d-%d %v\n", time.Since(ts), *addr, *addr+*length-1, v)
		time.Sleep(d)
	}
}
